<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "feed".
 *
 * @property integer $id
 * @property integer $author_id
 * @property string $text
 * @property integer $created_at
 * @property integer $last_edit_at
 * @property integer $post_id
 */
class Comment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'post_id', 'created_at', 'last_edit_at'], 'integer'],
            [['author_id', 'post_id', 'text', 'created_at'], 'required'],
            [['text'] ,'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'author_id' => 'Author ID',
            'post_id' => 'Post ID',
            'text' => 'Text',
            'created_at' => 'Created At',
            'last_edit_at' => 'Last Edit At',
        ];
    }


    public function getAuthor()
    {
        return User::findOne($this->author_id);
    }

    protected function reportsKey(){
        return self::class . ":{$this->id}:reports";
    }
    protected static function allReportedCommentsKey(){
        return self::class . ":reports";
    }

    public function report($user){
        $redis = Yii::$app->redis;
        $redis->sadd($this->reportsKey(), $user->id);
        $redis->sadd(self::allReportedCommentsKey(), $this->id);
        return true;
    }

    public function getUsersThatReported(){
        $ids =  Yii::$app->redis->smembers($this->reportsKey());
        return User::findAll($ids);
    }

    static public function getReportedComments(){
        $ids = Yii::$app->redis->smembers(self::allReportedCommentsKey());
        return Comment::findAll($ids);
    }

    public function deleteReportedComment(){
        $redis = Yii::$app->redis;
        $redis->delete($this->reportsKey());
        $redis->srem($this->allReportedCommentsKey(), $this->id);
        return $this->delete();
    }
}
