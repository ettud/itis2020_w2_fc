<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\redis\Connection;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $about
 * @property int $type
 * @property string $nickname
 * @property string $picture
 * @property string $password write-only password
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;

    const DEFAULT_IMAGE = '/img/profile_default_image.jpg';


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => time(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $username
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token)
    {
        return static::findOne([
            'verification_token' => $token,
            'status' => self::STATUS_INACTIVE
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Generates new token for email verification
     */
    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * БЭД практик)
     *
     * @return int|string
     */
    public function getNickname()
    {
        return $this->nickname ?? $this->id;
    }

    /** Подписаться
     *
     * @param User $user
     * @return bool
     */
    public function followUser(User $user)
    {
        $key1 = self::class . ":{$this->id}:subscriptions";
        $key2 = self::class . ":{$user->id}:followers";

        /** @var Connection $redis */
        $redis = Yii::$app->redis;

        $redis->sadd($key1, $user->id);
        $redis->sadd($key2, $this->id);

        return true;
    }

    /**
     * Отписаться
     *
     * @param User $user
     * @return bool
     */
    public function unFollowUser(User $user)
    {
        $key1 = self::class . ":{$this->id}:subscriptions";
        $key2 = self::class . ":{$user->id}:followers";

        /** @var Connection $redis */
        $redis = Yii::$app->redis;

        $redis->srem($key1, $user->id);
        $redis->srem($key2, $this->id);

        return true;
    }

    /**
     * Подписан ли на данного пользователя
     *
     * @param User $user
     * @return bool
     */
    public function checkIfFollowsUser(User $user)
    {
        $key = self::class . ":{$this->id}:subscriptions";

        $ids =  Yii::$app->redis->smembers($key);

        return in_array($user->id, $ids);
    }

    /**
     * Получить подписчиков
     *
     * @return mixed
     */
    public function getSubscriptions()
    {
        $key = self::class . ":{$this->id}:subscriptions";

        $ids =  Yii::$app->redis->smembers($key);

        return self::findAll($ids);
    }

    /**
     * Получить подписавшихся
     *
     * @return mixed
     */
    public function getFollowers()
    {
        $key = self::class . ":{$this->id}:followers";

        $ids = Yii::$app->redis->smembers($key);

        return self::findAll($ids);
    }

    /**
     * Число подсавшихся
     *
     * @return mixed
     */
    public function countFollowers()
    {
        return Yii::$app->redis->scard(self::class . ":{$this->id}:followers");
    }

    /**
     * Число подписок
     *
     * @return mixed
     */
    public function countSubscriptions()
    {
        return Yii::$app->redis->scard(self::class . ":{$this->id}:subscriptions");
    }

    /**
     * Общие друзья
     *
     * @param User $user
     * @return User[]
     */
    public function getMutualSubscriptionsTo(User $user)
    {
        /** @var string $key1 Мои подписки  */
        $key1 = self::class . ":{$this->id}:subscriptions";
        /** @var string $key2 Подписчики пользователя */
        $key2 = self::class . ":{$user->id}:followers";

        $diffIds = Yii::$app->redis->sinter($key1, $key2);

        return self::findAll($diffIds);
    }

    /**
     * @return string
     */
    public function getPicture()
    {
        if ($this->picture) {
            return Yii::$app->storage->getFile($this->picture);
        }
        return self::DEFAULT_IMAGE;
    }

    /**
     * Get data for newsfeed
     * @param integer $limit
     * @return array
     */
    public function getFeed(int $limit)
    {
        $order = ['post_created_at' => SORT_DESC];
        return $this->hasMany(Feed::className(), ['user_id' => 'id'])->orderBy($order)->limit($limit)->all();
    }

    public function getPersonalFeed(int $limit)
    {
        $order = ['created_at' => SORT_DESC];
        return $this->hasMany(Post::className(), ['user_id' => 'id'])->orderBy($order)->limit($limit)->all();
    }
}
