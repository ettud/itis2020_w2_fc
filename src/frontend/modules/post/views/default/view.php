<?php
/* @var $this yii\web\View */
/* @var $post common\models\Post */

use frontend\modules\post\assets\LikeAsset;
use yii\helpers\Html;
use yii\web\JqueryAsset;

LikeAsset::register($this);
?>
    <div class="post-default-index">

        <div class="block-post  row center-block">
            <div class="col-md-12 center-block">
                <div class="thumbnail">
                    <div class="media" style="float: left; padding-bottom: 10px;">
                        <div style="display: inline-block">
                            <?php if ($post->user): ?>
                                <img src="<?php echo $post->user->getPicture(); ?>" class="mr-3 img-circle" alt="user" style="width: 50px;">

                                <span class="mt-0 mb-1">
                                        <?php echo $post->user->username; ?>
                                </span>
                            <?php endif; ?>
                        </div>
                    </div>

                    <img src="<?php echo $post->getImage(); ?>"  class="card-img-top" alt="cat"
                         style="width: 100%;">
                    <div class="caption">
                        <ul class="list-unstyled" style="padding-bottom: 0px;margin-bottom: 5px;">
                            <li class="media">
                                <div>
                                    Лайков: <?php echo $post->countLikes(); ?>
                                    <?php if ($currentUser): ?>
                                        <a href="#" class="btn btn-primary button-unlike <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "" : "display-none"; ?>" data-id="<?php echo $post->id; ?>">
                                            Unlike&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-down"></span>
                                        </a>
                                        <a href="#" class="btn btn-primary button-like <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "display-none" : ""; ?>" data-id="<?php echo $post->id; ?>">
                                            Like&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span>
                                        </a>
                                    <?php endif; ?>
                                </div>

                                <div class="media-body" style="padding-left: 15px; padding-top: 7px;">
                                    <p class="card-text">
                                        <?php echo Html::encode($post->description); ?>
                                    </p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>



        <br>
        <h3 style="padding: 20px;"> Комментарии </h3>

        <script>
            function startEditingComment(event, comment_id){
                if(event.srcElement){
                    const button = event.srcElement;
                    const input = document.createElement("input");
                    const send = document.createElement("button");
                    send.innerText = "Изменить";
                    send.onclick = function(){
                        const request = new XMLHttpRequest();
                        request.open('post', '/post/comment/edit');
                        request.onreadystatechange=function(){
                            if (request.status==200){
                                location.reload();
                            }
                        }
                        const data = new FormData();
                        data.append("comment_id", comment_id);
                        data.append("text", input.value);
                        data.append("<?=Yii::$app->request->csrfParam; ?>", "<?=Yii::$app->request->getCsrfToken(); ?>");
                        request.send(data);
                    };
                    const parentDiv = button.parentNode;
                    button.style.display = 'none';
                    parentDiv.append(input);
                    parentDiv.append(send);
                }
            };
        </script>

        <?php foreach($post->getComments() as $comment): ?>
            <?php $author = $comment->getAuthor()?>
            <div class="media">
                <div class="media-left">
                    <img src="<?php echo $author->getPicture()?>" class="media-object mr-3 rounded-circle avatar-user-70" alt="user">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo Html::encode($author->username) .  ' (' . date("d.m.y h:i", $comment->created_at) .')'; ?> </h4>
                    <?php if($currentUser):?>
                        <a href="<?php echo '/post/comment/report?comment_id=' . $comment->id?>">Пожаловаться</a>
                        <?php if($currentUser->id == $author->id):?>
                        <div>
                            <button onclick="startEditingComment(event, <?php echo $comment->id?>)">Отредактировать комментарий</button>
                        </div>
                            <a href="<?php echo '/post/comment/delete?comment_id=' . $comment->id?>">Удалить комментарий</a>
                        <?php endif;?>
                    <?php endif;?>
                    <p class="card-text">
                        <?php echo Html::encode($comment->text); ?>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>

        <?php if(!(Yii::$app->user->isGuest)): ?>
            <script>
                function sendComment(event){
                    event.preventDefault();
                    //GTM-WV3BG69ga('send', 'event', 'comment', 'create');
                    const form = document.getElementById("send-comment-form");
                    const formData = new FormData(form);
                    const request = new XMLHttpRequest();
                    request.open(form.attributes.method.value, form.attributes.action.value);
                    request.onreadystatechange=function(){
                        if (request.status==200){
                            $("#send-comment-success").show();
                            $("#send-comment-fail").hide();
                            location.reload();
                        }
                        else{
                            $("#send-comment-success").hide();
                            $("#send-comment-fail").show();
                        }
                    }
                    request.send(formData);
                    return false;
                };

                function forceLower(strInput) {
                    strInput.value = strInput.value.toLowerCase();
                };
            </script>

            <span class="button-group-profile">
        <div class="alert alert-success display-none" id="send-comment-success">Комментарий отправлен</div>
        <div class="alert alert-danger display-none" id="send-comment-fail"></div>

        <form id="send-comment-form" action="<?php echo '/post/comment/create?post_id=' . $post->id?>" method="post" onsubmit="return sendComment(event);">
            <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" >
            <input name="text" >
            <button id="send-comment-form-submit-button" class="btn btn-outline-primary">Отправить</button>
        </form>
    </span>
        <?php endif; ?>

    </div>

    </div>

