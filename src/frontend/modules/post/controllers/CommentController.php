<?php

namespace frontend\modules\post\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use common\models\Post;
use frontend\modules\post\models\forms\PostForm;
use common\models\Comment;

/**
 * Default controller for the `post` module
 */
class CommentController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'edit', 'delete', 'report'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionCreate($post_id)
    {
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        $text = Yii::$app->request->post('text');

        if ($text) {

            $comment = new Comment();
            $currentUser = Yii::$app->user->identity;
            $comment->author_id=$currentUser->id;
            $comment->text=$text;
            $comment->created_at=time();
            $comment->post_id=$post_id;

            if ($comment->save()) {

                Yii::$app->session->setFlash('success', 'Comment created!');
                return true;
            }
        }

        return false;
    }

    public function actionDelete($comment_id){
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        $comment = Comment::findOne($comment_id);
        if($comment){
            if(Yii::$app->user->identity->id == $comment->author_id || Yii::$app->user->identity->type==1){
                if($comment->delete()){
                    Yii::$app->session->setFlash('success', 'Comment deleted!');
                    return true;
                }
            }
        }
        return false;
    }

    public function actionEdit(){
        $STDOUT = fopen("php://stdout", "w");
        fwrite($STDOUT, "---------------");
        fwrite($STDOUT, ( Yii::$app->request->post('text')));
        fwrite($STDOUT, ( Yii::$app->request->post('comment_id')));
        fwrite($STDOUT, "---------------");
        fclose($STDOUT);
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        $text = Yii::$app->request->post('text');
        $comment_id = Yii::$app->request->post('comment_id');
        if($comment_id){
            if($text){
                $comment = Comment::findOne($comment_id);
                if($comment){
                    if(Yii::$app->user->identity->id == $comment->author_id) {
                        $comment->text = $text;
                        return $comment->save(false);
                    }
                }
            }
        }
        return false;
    }

    public function actionReport($comment_id){
        if (\Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }
        $comment = Comment::findOne($comment_id);
        if($comment){
            return $comment->report(Yii::$app->user->identity);
        }
        return false;
    }

}
