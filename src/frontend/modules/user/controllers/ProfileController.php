<?php

namespace frontend\modules\user\controllers;

use common\models\User;
use Faker\Factory;
use frontend\components\Storage;
use frontend\modules\user\models\forms\EditProfileForm;
use frontend\modules\user\models\forms\PictureForm;
use yii\web\Response;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use common\models\ImageResize;

/**
 * Profile controller for the `user` module
 */
class ProfileController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['view'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['subscribe', 'unsubscribe', 'generate', 'upload-picture', 'delete-picture', 'edit'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $nickname
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($nickname)
    {
        $user = $this->findModelByParam($nickname);
        $currentUser = Yii::$app->user->identity;
        $modelPicture = new PictureForm();
        $feedItems = $user->getPersonalFeed(10);

        return $this->render('view', compact('user', 'currentUser', 'modelPicture', 'feedItems'));
    }

    /**
     * @throws \yii\base\Exception
     */
    public function actionGenerate()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 1000; $i++) {
            $user = new User([
                'username' => $faker->name,
                'email' => $faker->email,
                'about' => $faker->text,
                'nickname' => $faker->regexify('[A-Za-z0-9_]{5,15}'),
                'auth_key' => Yii::$app->security->generateRandomString(),
                'password_hash' => Yii::$app->security->generateRandomString(),
            ]);
            $user->save(false);
        }
    }

    /**
     * @param $id
     * @throws NotFoundHttpException
     */
    public function actionSubscribe($id)
    {
        /** @var User $currentUser */
        $currentUser = Yii::$app->user->identity;
        /** @var User $user */
        $user = $this->findModel($id);

        // Подписываемся
        if ($currentUser->followUser($user)) {
            Yii::$app->session->setFlash('success', 'Подписка успешно произведена');
        } else {
            Yii::$app->session->setFlash('error', 'Ошибка подписки');
        }

        return $this->redirect(['profile/view', 'nickname' =>$id]);
    }

    public function actionUnsubscribe($id)
    {
        /** @var User $currentUser */
        $currentUser = Yii::$app->user->identity;
        /** @var User $user */
        $user = $this->findModel($id);

        // Подписываемся
        if ($currentUser->unFollowUser($user)) {
            Yii::$app->session->setFlash('success', 'Отписка успешно произведена');
        } else {
            Yii::$app->session->setFlash('error', 'Отписка подписки');
        }

        return $this->redirect(['profile/view', 'nickname' =>$id]);
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if (!$user = User::findOne($id)) {
            throw new NotFoundHttpException('user not found');
        }

        return $user;
    }

    /**
     * @param $param
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    private function findModelByParam($param)
    {
        if (!$user = User::find()->where(['nickname' => $param])->orWhere(['id' => $param])->one()) {
            throw new NotFoundHttpException('user not found');
        }

        return $user;
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionUploadPicture()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $model = new PictureForm();
        $model->picture = UploadedFile::getInstance($model, 'picture');

        if ($model->validate()) {
            $picturePath = $model->picture;
            $picturePath = Yii::$app->storage->saveUploadedFile($picturePath);
            {
                $maxWidth = Yii::$app->params['postPicture']['maxWidth'];
                $maxHeight = Yii::$app->params['postPicture']['maxHeight'];
                $resizer = new ImageResize(Yii::$app->storage->getStoragePath() . $picturePath);
                if ($resizer->getImageWidth() > $maxWidth ||
                    $resizer->getImageHeight() > $maxHeight) {
                    $resizer->resize($maxWidth, $maxHeight, true);
                }
            }

            $user = Yii::$app->user->identity;
            $user->picture = $picturePath;
            if ($user->save(false, ['picture'])) {
                return [
                    'success' => true,
                    'pictureUri' => Yii::$app->storage->getFile($user->picture),
                ];
            }
        }
        return ['success' => false, 'errors' => $model->getErrors()];
    }

    /**
     * @return array
     * @throws \yii\base\Exception
     */
    public function actionDeletePicture()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $user = Yii::$app->user->identity;
        $user->picture = null;
        if ($user->save(false, ['picture'])) {
            return [
                'success' => true
            ];
        }
        return ['success' => false];
    }

    public function actionEdit()
    {
        if (Yii::$app->request->post('nickname')) {
            $model = new EditProfileForm();
            $model->nickname = Yii::$app->request->post('nickname');
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($model->validate()) {
                $user = Yii::$app->user->identity;
                $user->nickname = $model->nickname;
                if ($user->save(false, ['nickname'])) {
                    return [
                        'success' => true
                    ];
                }
            }
            return [
                'success' => false
            ];
        }

        return $this->render('edit_profile.php');
    }
}
