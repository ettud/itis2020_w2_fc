<?php
/**
 * @var User $user
 * @var User $currentUser
 * @var \frontend\modules\user\models\forms\PictureForm $modelPicture
 */

use common\models\User;
use dosamigos\fileupload\FileUpload;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

?>

<div>
    <script>
        function editProfile(event){
            event.preventDefault();
            const form = document.getElementById("edit-profile-form");
            const formData = new FormData(form);
            const request = new XMLHttpRequest();
            request.open(form.attributes.method.value, form.attributes.action.value);
            request.onreadystatechange=function(){
                if (request.status==200){
                    $("#profile-edit-success").show();
                    $("#profile-edit-fail").hide();
                }
                else{
                    $("#profile-edit-success").hide();
                    $("#profile-edit-fail").show();
                }
            }
            request.send(formData);
            return false;
        };

        function forceLower(strInput) {
            strInput.value = strInput.value.toLowerCase();
        };
    </script>

    <span class="button-group-profile">
        <div class="alert alert-success display-none" id="profile-edit-success">Аватар обновлён</div>
        <div class="alert alert-danger display-none" id="profile-edit-fail"></div>

        <form id="edit-profile-form" action="/user/profile/edit" method="post" onsubmit="return editProfile(event);">
            <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" >
            <label for="nickname-input">Введите новый ник, используя только латиница</label>
            <input id="nickname-input" name="nickname" pattern="[a-zA-Z]+" onkeyup="return forceLower(this);">
            <button class="btn btn-outline-primary">Изменить</button>
        </form>
    </span>
</div>
