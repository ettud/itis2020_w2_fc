<?php
/**
 * @var User $user
 * @var User $currentUser
 * @var \frontend\modules\user\models\forms\PictureForm $modelPicture
 */

use common\models\User;
use dosamigos\fileupload\FileUpload;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\bootstrap\Modal;

?>

<div>
    <div style="display: inline-block; vertical-align: center;">
        <img id="profile-picture" src="<?php echo $user->getPicture(); ?>" class="mr-3 rounded-circle avatar-user-100" alt="user">
        <span style="font-weight: bold;"><?php echo Html::encode($user->username); ?></span>
    </div>

    <?php if ($currentUser): ?>
        <?php if ($currentUser->equals($user)): ?>
            <?php if ($user->getPicture()): ?>
                <script>
                    function deletePicture(event){
                        event.preventDefault();
                        const form = document.getElementById("delete-picture-form");
                        const formData = new FormData(form);
                        const request = new XMLHttpRequest();
                        request.open(form.attributes.method.value, form.attributes.action.value);
                        request.onreadystatechange=function(){
                            if (request.status==200){
                                location.reload();
                            }
                            else{
                                alert("Не удалось удалить аватар.");
                            }
                        }
                        request.send(formData);
                        return false;
                    }
                </script>
                <form id="delete-picture-form" action="/user/profile/delete-picture" method="post" onsubmit="return deletePicture(event);">
                    <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" >
                    <button class="btn btn-outline-primary">Удалить аватар</button>
                </form>
            <?php endif; ?>
            <span class="button-group-profile">
                <div class="alert alert-success display-none" id="profile-image-success">Аватар обновлён</div>
                <div class="alert alert-danger display-none" id="profile-image-fail"></div>
                <?= FileUpload::widget([
                    'model' => $modelPicture,
                    'attribute' => 'picture',
                    'url' => ['/user/profile/upload-picture'],
                    'options' => ['accept' => 'image/*'],
                    'clientEvents' => [
                        'fileuploaddone' => 'function(e, data) {
                            if (data.result.success) {
                                $("#profile-image-success").show();
                                $("#profile-image-fail").hide();
                                $("#profile-picture").attr("src", data.result.pictureUri);
                            } else {
                                $("#profile-image-fail").html(data.result.errors.picture).show();
                                $("#profile-image-success").hide();
                            }
                        }',
                    ],
                ]); ?>
                <button type="button" class="btn btn-outline-primary" onclick="location.href='/user/profile/edit';">Редактировать</button>
            </span>

            <hr/>

        <?php else: ?>

            <?php if ($currentUser->checkIfFollowsUser($user)): ?>
                <a href="<?php echo Url::to(['/user/profile/unsubscribe', 'id' => $user->getId()]); ?>" class="btn btn-info">Unsubscribe</a>
            <?php else: ?>
                <a href="<?php echo Url::to(['/user/profile/subscribe', 'id' => $user->getId()]); ?>" class="btn btn-info">Subscribe</a>
            <?php endif; ?>

            <?php if (count($currentUser->getMutualSubscriptionsTo($user)) != 0): ?>
                <hr>
                <h5>Friends, who are also following <?php echo Html::encode($user->username); ?>: </h5>
                <div class="row">
                    <?php foreach ($currentUser->getMutualSubscriptionsTo($user) as $item): ?>
                        <div class="col-md-12">
                            <a href="<?php echo Url::to([
                                '/user/profile/view',
                                'nickname' => ($item['nickname']) ? $item['nickname'] : $item['id'],
                            ]); ?>">
                                <?php echo Html::encode($item['username']); ?>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    <hr/>


    <?php
    Modal::begin([
        'header' => '<h2>Подписчики</h2>',
        'toggleButton' => ['label' => ('Followers: ' . $user->countFollowers()), "class" => "btn btn-primary btn-lg"],
    ]);?>

        <div class="row">
            <?php foreach ($user->getFollowers() as $follower): ?>
                <div class="col-md-12">
                    <a href="<?php echo Url::to([
                        '/user/profile/view',
                        'nickname' => ($follower['nickname']) ? $follower['nickname'] : $follower['id'],
                    ]); ?>">
                        <?php echo Html::encode($follower['username']); ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

    <?php Modal::end(); ?>


    <?php
    Modal::begin([
        'header' => '<h2>Подписки</h2>',
        'toggleButton' => ['label' => ('Subscriptions: ' . $user->countSubscriptions()), "class" => "btn btn-primary btn-lg"],
    ]);?>

        <div class="row">
            <?php foreach ($user->getSubscriptions() as $follower): ?>
                <div class="col-md-12">
                    <a href="<?php echo Url::to([
                        '/user/profile/view',
                        'nickname' => ($follower['nickname']) ? $follower['nickname'] : $follower['id'],
                    ]); ?>">
                        <?php echo Html::encode($follower['username']); ?>
                    </a>
                </div>
            <?php endforeach; ?>
        </div>

    <?php Modal::end(); ?>

    <?php endif; ?>
    <div class="description-user">
        <p><?php echo HtmlPurifier::process($user->about); ?></p>
    </div>
    <hr>


    <?php if ($feedItems): ?>
        <?php foreach ($feedItems as $post): ?>
            <div class="block-post  row center-block">
                <div class="col-md-12 center-block">
                    <div class="thumbnail">
                        <div class="media" style="float: left; padding-bottom: 10px;">
                            <div style="display: inline-block">
                                <?php if ($post->user): ?>
                                    <img src="<?php echo $post->user->getPicture(); ?>" class="mr-3 img-circle" alt="user" style="width: 50px;">

                                    <span class="mt-0 mb-1">
                                        <?php echo $post->user->username; ?>
                                </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <img src="<?php echo $post->getImage(); ?>"  class="card-img-top" alt="cat"
                             style="width: 100%;">
                        <div class="caption">
                            <ul class="list-unstyled" style="padding-bottom: 0px;margin-bottom: 5px;">
                                <li class="media">
                                    <div>
                                        Лайков: <?php echo $post->countLikes(); ?>
                                        <?php if ($currentUser): ?>
                                            <a href="#" class="btn btn-primary button-unlike <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "" : "display-none"; ?>" data-id="<?php echo $post->id; ?>">
                                                Unlike&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-down"></span>
                                            </a>
                                            <a href="#" class="btn btn-primary button-like <?php echo ($currentUser && $post->isLikedBy($currentUser)) ? "display-none" : ""; ?>" data-id="<?php echo $post->id; ?>">
                                                Like&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span>
                                            </a>
                                        <?php endif; ?>
                                    </div>

                                    <div class="media-body" style="padding-left: 15px; padding-top: 7px;">
                                        <p class="card-text">
                                            <?php echo Html::encode($post->description); ?>
                                            <?php $commentCount = $post->countComments();?>
                                            <?php if($commentCount > 0):?>
                                                <a href="<?php echo '/post/' . $post->id?>"> (Откройте и прочтите комментарии, их там аж <?php echo $commentCount?>!) </a>
                                            <?php else: ?>
                                                <a href="<?php echo '/post/' . $post->id?>"> (Откройте и оставьте комментарий первым!) </a>
                                            <?php endif;?>
                                        </p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="col-md-12">
            Nobody posted yet!
        </div>
    <?php endif; ?>
    </div>
</div>
