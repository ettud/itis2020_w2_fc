<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;

class EditProfileForm extends Model
{
    /**
     * @var string
     */
    public $nickname;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['nickname'], 'string' ],
            ['nickname', 'match', 'pattern' => '/[a-z]+/']
        ];
    }
}
