<?php

namespace frontend\modules\user\models\forms;

use Yii;
use yii\base\Model;

class PictureForm extends Model
{
    /**
     * @var string
     */
    public $picture;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['picture'], 'file',
                'extensions' => ['jpg', 'png'],
                'checkExtensionByMimeType' => true,
                'maxSize' => Yii::$app->params['maxFileSize']
            ],
        ];
    }
}
