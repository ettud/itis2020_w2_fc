<?php
namespace frontend\controllers;

use common\models\User;
use Yii;
use yii\web\Controller;

use yii\web\Cookie;
use common\models\Comment;

/**
 * Admin controller
 */
class AdminController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        if($currentUser->type != 1){
            return false;
        }

        $limit = Yii::$app->params['feedPostLimit'];
        $feedItems = $currentUser->getFeed($limit);

        return $this->render('index', [
            'feedItems' => $feedItems,
            'currentUser' => $currentUser,
        ]);
    }

    public function actionReportedComments()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['/user/default/login']);
        }

        /* @var $currentUser User */
        $currentUser = Yii::$app->user->identity;
        if($currentUser->type != 1){
            return false;
        }

        return $this->render('reported_comments', [
            'comments' => Comment::getReportedComments(),
        ]);
    }
}
