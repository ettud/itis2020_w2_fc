<?php

/* @var $this yii\web\View */

use yii\web\JqueryAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

use common\models\Post;


/* @var $this yii\web\View */
/**
 * @var \common\models\Comment [] $comments
 */
$this->title = 'Жалобы на комментарии';
?>
    <div>
        <?php foreach($comments as $comment): ?>
            <?php $author = $comment->getAuthor()?>
            <div class="media">
                <div class="media-left">
                    <img src="<?php echo $author->getPicture()?>" class="media-object mr-3 rounded-circle avatar-user-70" alt="user">
                </div>
                <div class="media-body">
                    <h4 class="media-heading"><?php echo Html::encode($author->username) .  ' (' . date("d.m.y h:i", $comment->created_at) .')'; ?> </h4>
                    <a href="<?php echo '/post/comment/delete?comment_id=' . $comment->id?>">Удалить комментарий</a>
                    <p class="card-text">
                        <?php echo Html::encode($comment->text); ?>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>