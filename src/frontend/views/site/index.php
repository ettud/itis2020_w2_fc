<?php

/* @var $this yii\web\View */

use yii\web\JqueryAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

use common\models\Post;


/* @var $this yii\web\View */
/**
 * @var \common\models\User [] $users
 */
$this->title = 'Лента';
?>
<div class="site-index">
    <?php if ($feedItems): ?>
        <?php foreach ($feedItems as $feedItem): ?>
            <?php /* @var $feedItem Feed */ ?>

            <div class="col-md-12">

                <div class="col-md-12">
                    <img src="<?php echo $feedItem->author_picture; ?>" width="30" height="30"/>
                    <a href="<?php echo Url::to(['/user/profile/view', 'nickname' => ($feedItem->author_nickname) ? $feedItem->author_nickname : $feedItem->author_id]); ?>">
                        <?php echo Html::encode($feedItem->author_name); ?>
                    </a>
                </div>

                <img src="<?php echo Yii::$app->storage->getFile($feedItem->post_filename); ?>" style="width: 300px;" />
                <div class="col-md-12">
                        <p class="card-text">
                            <?php echo HtmlPurifier::process($feedItem->post_description); ?>
                            <?php $commentCount = $feedItem->getPost()->countComments();?>
                            <?php if($commentCount > 0):?>
                                <a href="<?php echo '/post/' . $feedItem->post_id?>"> (Откройте и прочтите комментарии, их там аж <?php echo $commentCount?>!) </a>
                            <?php else: ?>
                                <a href="<?php echo '/post/' . $feedItem->post_id?>"> (Откройте и оставьте комментарий первым!) </a>
                            <?php endif;?>
                        </p>
                </div>

                <div class="col-md-12">
                    <?php echo Yii::$app->formatter->asDatetime($feedItem->post_created_at); ?>
                </div>

                <div>
                    Лайков: <?php echo $feedItem->countLikes(); ?>
                    <?php if ($currentUser): ?>
                        <?php $isLiked = $feedItem->getPost()->isLikedBy($currentUser)?>
                        <a href="#" class="btn btn-primary button-unlike <?php echo ($isLiked) ? "" : "display-none"; ?>" data-id="<?php echo $feedItem->post_id; ?>">
                            Unlike&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-down"></span>
                        </a>
                        <a href="#" class="btn btn-primary button-like <?php echo ($isLiked) ? "display-none" : ""; ?>" data-id="<?php echo $feedItem->post_id; ?>">
                            Like&nbsp;&nbsp;<span class="glyphicon glyphicon-thumbs-up"></span>
                        </a>
                    <?php endif; ?>
                </div>

            </div>
            <div class="col-md-12"><hr/></div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="col-md-12">
            Nobody posted yet!
        </div>
    <?php endif; ?>

</div>

<?php $this->registerJsFile('@web/js/likes.js', [
    'depends' => JqueryAsset::className(),
]);
