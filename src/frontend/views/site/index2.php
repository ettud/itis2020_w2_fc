?php

/* @var $this yii\web\View */
/**
 * @var \common\models\User [] $users
 */
$this->title = 'Лента';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">
        <?php foreach ($users as $user): ?>
            <div class="">
                <?= \yii\helpers\Html::a($user->username, ['/user/profile/view', 'nickname' => $user->getNickname()]); ?>
            </div>
            <hr>
        <?php endforeach; ?>
    </div>
</div>
