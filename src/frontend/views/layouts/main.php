<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);

$allowLocalizationChange = false;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8 image-header-block center-block ">
                <a href="/">
                    <img src="/img/head.png" class="image-header">
                </a>

                <?php if($allowLocalizationChange):?>
                    <div class="col-md-2">
                        <?= Html::beginForm(['/site/language']) ?>
                        <?= Html::dropDownList('language', Yii::$app->language, ['en-US' => 'English', 'ru-RU' => 'Русский']) ?>
                        <?= Html::submitButton('Change') ?>
                        <?= Html::endForm() ?>
                    </div>
                <?php endif; ?>


                <ul class="nav nav-pills center-block" style="margin: 0 auto;">
                    <li class="nav-item">
                        <a class="nav-link active" href="/site/index">Главная</a>
                    </li>
                    <?php if (Yii::$app->user->isGuest): ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/user/default/login">Войти</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/user/default/signup">Зарегистрироваться</a>
                        </li>
                    <?php else: ?>
                        <li class="nav-item">
                            <a class="nav-link" href="/site/my-profile"><?php echo Yii::t('menu', 'My profile')?></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/post/default/create">Добавить пост</a>
                        </li>
                        <li class="nav-item">
                            <form id="logout_form" action="/user/default/logout" method="post">
                                <input type="hidden" name="<?=Yii::$app->request->csrfParam; ?>" value="<?=Yii::$app->request->getCsrfToken(); ?>" >
                                <a class="nav-link" href="#" onclick="document.getElementById('logout_form').submit();">Выйти</a>
                            </form>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="col-md-2"></div>
        <div class="col-md-8 image-header-block center-block">

            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
        <div class="col-md-2 "></div>
    </div>
</div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
