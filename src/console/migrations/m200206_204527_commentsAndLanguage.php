<?php

use yii\db\Migration;

/**
 * Class m200206_204527_commentsAndLanguage
 */
class m200206_204527_commentsAndLanguage extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('comment', 'post_id', $this->integer()->notNull());
        $array = array('DATABASE imagesdb', 'TABLE comment', 'TABLE post');
        foreach($array as $target){
            $this->execute('ALTER ' . $target . ' CHARACTER SET utf8 COLLATE utf8_unicode_ci;');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->removeColumn('comment', 'post_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200206_204527_commentsAndLanguage cannot be reverted.\n";

        return false;
    }
    */
}
